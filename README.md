# toxprofileR

This is a collection of R functions to create toxicogenomic fingerprints. This package is the first version of the package, as it was published in [Schüttler et al. 2019](https://doi.org/10.1093/gigascience/giz057). For the new version of the package, which incorporates additional functionalities, see [toxprofileR2](https://git.ufz.de/iTox/toxprofiler2).

To install the package, you need to install the package devtools first:

```r
install.packages("devtools")
```

Afterwards you can install toxprofileR.

```r
devtools::install_git("https://git.ufz.de/itox/toxprofileR.git")
```

Please note: There might be difficulties installing dependencies via Rstudio. If problems arise, please directly install package from R.
